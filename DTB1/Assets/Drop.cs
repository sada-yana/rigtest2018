﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;


public class Drop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private UnityEngine.Events.UnityEvent DropBegin,DropOut;

    //このコードはUIのボタン以外をクリックorタッチをすると呼び出される

        //タッチした瞬間（InputのDownてきな）
    public void OnBeginDrag(PointerEventData eventData)
    {
        DropBegin.Invoke();
    }
    //タッチしている間
    public void OnDrag(PointerEventData eventData)
    {
       // Debug.Log("test1");
    }

    //放した瞬間間（InputのUpてきな）
    public void OnEndDrag(PointerEventData eventData)
    {
        DropOut.Invoke();
    }

}