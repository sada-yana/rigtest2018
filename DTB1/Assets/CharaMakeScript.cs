﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharaMakeScript : MonoBehaviour {

    
    //ランダム生成するオブジェクト
    public GameObject[] Chara;
    //設置したオブジェクトのRigidbody2D（すべてのオブジェクトが停止しているかどうか判定用）
    private List<Rigidbody2D> CharaAllRidtmp=new List<Rigidbody2D>();
    //今設置したオブジェクトのtmp用
    private Rigidbody2D CharaTmpRid;
    private Transform CharaTmpTras;

    
    //ゲーム進行
    private int isMove=0;
    //置いた回数
    private int Num;
    //自分or相手のターンかどうか
    private bool IsPlayer;
    //タイマー（tmp用）
    private float Timer;
    
    //UI表示テキスト
    [SerializeField]
    private Text PlayerText, NumText, TimerText,EndText;
    //ゲーム終了時のボタン
    [SerializeField]
    private GameObject GameEndUI;
    //カメラ初期位置Y座標
    private float tmppositionY;

    //ゲーム全体進行
    private void GameManage()
    {
        switch (isMove)
        {
            case 0: CharaAllRidtmp.Clear(); isMove = 1; IsPlayer = (int)Random.Range(0, 2) == 0;Num = 0; tmppositionY = transform.position.y; break;
            case 1: isMove = 2;  MakeRandom(); Timer=0; break;
            case 2: if (IsTimer(10)) { isMove = 4; };  break;
            case 3: if (IsTimer(10)) { isMove = 4; };  ScreenTap(); break;
            case 4: CharaTmpRid.simulated = true; isMove = 5; Timer = 0;Num++; break;
            case 5: if (IsTimer(1)) { if (TrasCheck()) { isMove = 6;  }} break;
            case 6: if (Mathf.Abs(transform.position.y - CharaTmpTras.position.y) < 3) {  transform.position += Vector3.up * Time.deltaTime; } else { isMove = 1; IsPlayer = !IsPlayer; } break;
            case 7: if (transform.position.y - tmppositionY > 0) { transform.position -= Vector3.up * Time.deltaTime; } break;
            default: break;
        }
    }

    //時間カウントタイマー
    private bool IsTimer(float time)
    {
        Timer +=Time.deltaTime;
        return (Timer > time);
    }

    //オブジェクトランダム生成＋キャッシュ
    private void MakeRandom()
    {
       GameObject CharaTmp = (GameObject)Instantiate(Chara[Random.Range(0, Chara.Length)], transform.position, transform.rotation);
        CharaTmpRid = CharaTmp.GetComponent<Rigidbody2D>();
        CharaTmpTras = CharaTmp.transform;
        CharaAllRidtmp.Add(CharaTmpRid);
    }

    //ボタンを押すと45度回転
    public void IsRotation()
    {
        if (isMove == 2 || isMove == 3)
            CharaTmpTras.transform.eulerAngles += new Vector3(0, 0, 45);
    }

    //Dropから画面に触れているか判定
    public void TouchCheck(bool On)
    {
        if (On)
        {
          if(isMove==2) isMove = 3;
        }
        else
        {
           if(isMove==3) isMove = 4;
        }
    }

    //タップ位置にオブジェクト移動
    public void ScreenTap()
    {
        //Vector3tmp
        Vector3 worldPos =Vector3.zero;
        //スマホ等のタッチorマウスクリック（PC）
        bool touch =true;
        if (Input.GetKey(KeyCode.Mouse0))
        {
            touch = false;
        }

        //
        if (touch)
        {
            worldPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        else
        {
            worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        CharaTmpTras.position = new Vector3(worldPos.x, CharaTmpTras.position.y, 0);
    }

    //位置が変わらないまでチェック（停止判定）
    private bool TrasCheck()
    {
        bool check = true;
        //先に今落としたオブジェクトだけ判定することで無駄な全体ループ処理をなくす（最適化）
        if (CharaTmpRid.velocity.magnitude > 0)
        {
            check = false;
        }
        else
        {
            foreach (Rigidbody2D value in CharaAllRidtmp)
            {
                if (value.velocity.magnitude > 0)
                {
                    check = false;
                    break;
                }
            }
        }
        return check;
    }

    //ゲーム終了
    public void GameEnd()
    {
        if (isMove != 7)
        {
            isMove = 7;
            EndText.text = IsPlayer ? "相手の勝ち" : "自分の勝ち";
            GameEndUI.SetActive(true);
        }
    }


    //UI更新 前と比較して変更されていたら更新（）
    private void IsUI()
    {
        if (isMove == 2 || isMove == 3)
        {
          if(TimerText.text!= (10 - (int)Timer).ToString())  TimerText.text = (10 - (int)Timer).ToString();
        }
        else
        {
          if (TimerText.text != "")  TimerText.text = "";
        }

        if (isMove != 0)
        {
           if(NumText.text != Num.ToString()) NumText.text = Num.ToString();
        }
        else
        {
           if(NumText.text != "") NumText.text = "";
        }

        if(isMove != 0 && isMove != 7)
        {
          if (PlayerText.text !=( IsPlayer ? "自分の番" : "相手の番"))  PlayerText.text = IsPlayer ? "自分の番" : "相手の番";
        }
        else
        {
           if(PlayerText.text != "") PlayerText.text = "";
        }
    }

    //シーン移動
    public void SceanLoad(string name)
    {
        SceneManager.LoadScene(name);
    }

    // Use this for initialization
    void Start () {
        isMove = 0;

    }
	
	// Update is called once per frame
	void Update () {
        GameManage();
        IsUI();
    }

    
}
